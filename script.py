# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals
#from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words
from sumy.summarizers.lex_rank import LexRankSummarizer #We're choosing Lexrank, other algorithms are also built in


#https://pypi.python.org/pypi/sumy

LANGUAGE = "spanish"
SENTENCES_COUNT = 2


if __name__ == "__main__":
    #url = "http://www.bbc.com/news/world-us-canada-42671152?intlink_from_url=http://www.bbc.com/news/topics/cp7r8vgl2lgt/donald-trump&link_location=live-reporting-story"
    #parser = HtmlParser.from_url(url, Tokenizer(LANGUAGE))
    # or for plain text files
    parser = PlaintextParser.from_file("doc.txt", Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)

    summarizer = Summarizer(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)

    for sentence in summarizer(parser.document, SENTENCES_COUNT):
        print(sentence)

    

    #LexRankSumm
    #https://gist.github.com/lastlegion/dd7f11aada4673dfbb4b
    file = "doc.txt"  # name of the plain-text file
    parser = PlaintextParser.from_file(file, Tokenizer("spanish"))
    summarizer2 = LexRankSummarizer()

    summary = summarizer(parser.document, 2)  # of sentences

    for sentence in summary:
        print(sentence)